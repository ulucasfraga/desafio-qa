
class WhatsAppElementos < SitePrism::Page

  ###Criar grupo - Elementos
  element :btn_menu, ".rAUz7 div[title='Menu']"
  element :btn_novo_grupo, "div[title='Novo grupo']"
  element :input_nome_contato, "input[placeholder='Digite nome do contato']"
  element :btn_add_contato, "._3hV1n"
  element :input_nome_grupo, "._1DTd4 div[class='pluggable-input-body copyable-text selectable-text']"
  element :btn_criar_grupo, "._3hV1n"

  ###Excluir grupo - elementos
  element :chat_desafio, ".chat-title span[title=Desafio-QA]"
  element :btn_menu_grupo, ".pane-chat-controls .rAUz7 div[title='Menu']"
  element :btn_sair_grupo, "div[title='Sair do grupo']"
  element :popup_grupo, "div[class='_1WZqU PNlAR']"
  element :btn_apagar_grupo, "div[title='Apagar grupo']"

  ###Enviar mensagem grupo - elementos
  element :input_mensagem, ".input-container div[class='pluggable-input-body copyable-text selectable-text']"
end
