# encoding: utf-8
# !/usr/bin/env ruby

require 'rspec/expectations'

class CriarGrupo
  include Capybara::DSL
  include RSpec::Matchers

  def criar_novo_grupo(nome_contato, nome_grupo)
    @elementos = WhatsAppElementos.new
    @elementos.btn_menu.click
    @elementos.btn_novo_grupo.click
    @elementos.input_nome_contato.set nome_contato
    @elementos.input_nome_contato.native.send_keys(:enter)
    @elementos.btn_add_contato.click
    @elementos.input_nome_grupo.set nome_grupo
    @elementos.btn_criar_grupo.click
  end
end

class ExcluirGrupo
  include Capybara::DSL
  include RSpec::Matchers

  def selecionar_grupo
    @elementos = WhatsAppElementos.new
    expect(page).to have_css('.chat-title span[title=Desafio-QA]', :visible => true)
    @elementos.first('.chat-title span[title=Desafio-QA]').click
  end

  def sair_grupo
    @elementos.btn_menu_grupo.click
    @elementos.btn_sair_grupo.click
    expect(page).to have_css("div[class='_1WZqU PNlAR']", :visible => true)
    @elementos.popup_grupo.click
  end

  def excluir_grupo
    @elementos.btn_menu_grupo.click
    @elementos.btn_apagar_grupo.click
    expect(page).to have_css("div[class='_1WZqU PNlAR']", :visible => true)
    @elementos.popup_grupo.click
  end
end

class EnviarMensagemGrupo
  include Capybara::DSL
  include RSpec::Matchers

  def enviar_mensagem_grupo(mensagem)
    @elementos = WhatsAppElementos.new
    @elementos.input_mensagem.set mensagem
    @elementos.input_mensagem.native.send_keys(:enter)
  end
end
