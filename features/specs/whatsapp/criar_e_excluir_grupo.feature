# language: pt

@criar_e_excluir
Funcionalidade: Criar e excluir grupo
 Como usuario do whatsapp
 Quero criar e excluir um novo grupo de conversa

@criar_grupo
Cenario: Criar novo grupo no whatsapp
  Dado que eu esteja na pagina inicial
  Quando eu criar um novo grupo
  Entao eu vejo o grupo criado com sucesso

@excluir_grupo
Cenario: Excluir novo grupo no whatsapp
  Dado que eu esteja na pagina inicial
  Quando eu excluir o novo grupo
  Entao eu vejo o grupo excluido com sucesso
