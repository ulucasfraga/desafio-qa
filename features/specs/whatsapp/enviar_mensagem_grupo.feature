# language: pt

@enviar_mensagem_grupo
Funcionalidade: Enviar mensagem no grupo
 Como usuario do whatsapp
 Quero enviar mensagens no grupo
 Para que eu possa me comunicar com meus contatos

Contexto: Criar novo grupo
  Dado que eu esteja na pagina inicial
  Quando eu criar um novo grupo

@mensagem_grupo
@apagar_grupo
Cenario: Enviar nova mensagem para um grupo do whatsapp
  Dado que eu escolha o novo grupo
  Quando eu enviar uma nova mensagem
  Entao eu vejo mensagem enviada com sucesso
