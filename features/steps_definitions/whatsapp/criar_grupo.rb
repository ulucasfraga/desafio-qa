Dado("que eu esteja na pagina inicial") do
  visit('/')
end

Quando("eu criar um novo grupo") do
  @wapp = CriarGrupo.new
  @wapp.criar_novo_grupo("Naiane Mistura", "Desafio-QA")
end

Entao("eu vejo o grupo criado com sucesso") do
  expect(page).to have_css(".chat-title span", :text => "Desafio-QA")
end
