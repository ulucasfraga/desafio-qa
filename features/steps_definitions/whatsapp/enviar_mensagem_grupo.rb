Dado("que eu escolha o novo grupo") do
  #apenas para fazer sentido o BDD
end

Quando("eu enviar uma nova mensagem") do
  @wapp = EnviarMensagemGrupo.new
  @wapp.enviar_mensagem_grupo("Mensagem para o grupo Desafio-QA")
end

Entao("eu vejo mensagem enviada com sucesso") do
  expect(page).to have_css("div[class='_3zb-j ZhF0n'] span", :text => "Mensagem para o grupo Desafio-QA")
end
