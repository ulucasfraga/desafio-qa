Quando("eu excluir o novo grupo") do
  @wapp = ExcluirGrupo.new
  @wapp.selecionar_grupo
  @wapp.sair_grupo
  @wapp.excluir_grupo
end

Entao("eu vejo o grupo excluido com sucesso") do
  expect(page).to have_css(".hYvJ8 span", :text => "Grupo apagado")
end
